#!/bin/sh

# Update & Upgrade
#apt update -y
#apt upgrade -y

# Install StrongSwan
#apt install strongswan strongswan-pki

# Setting up the certificate authority
sudo mkdir -p ~/pki/cacerts
sudo mkdir -p ~/pki/certs
sudo mkdir -p ~/pki/private
chmod 700 ~/pki

ipsec pki --gen --type rsa --size 4096 --outform pem > ~/pki/private/ca-key.pem
ipsec pki --self --ca --lifetime 3650 --in ~/pki/private/ca-key.pem \
    --type rsa --dn "CN=VPN root CA" --outform pem > ~/pki/cacerts/ca-cert.pem
ipsec pki --gen --type rsa --size 4096 --outform pem > ~/pki/private/server-key.pem

# Generating VPN Server Certs
ipsec pki --pub --in ~/pki/private/server-key.pem --type rsa \
    | ipsec pki --issue --lifetime 1825 \
        --cacert ~/pki/cacerts/ca-cert.pem \
        --cakey ~/pki/private/ca-key.pem \
        --dn "CN=$1" --san "$1" \
        --flag serverAuth --flag ikeIntermediate --outform pem \
    >  ~/pki/certs/server-cert.pem

cp -r ~/pki/* /etc/ipsec.d/

# Replacing left side id with public ip address arument
sed -i "s/.*leftid=.*/    leftid=$1/g" "ipsec.conf"
sed -i "s/.*rightid=.*/    rightid=$2/g" "ipsec.conf" 

sudo mv /etc/ipsec.conf /etc/ipsec.conf.original
sudo cp ipsec.conf /etc/ipsec.conf

# Replacing EAP password with user defined one - NOTE SED is delimited by # symbols due to wanting to escape characters
sudo sed -i "s#.*: EAP.*#$3 : EAP \"$4\"#g" "ipsec.secrets"
sudo mv /etc/ipsec.secrets /etc/ipsec.secrets.original
sudo cp ipsec.secrets /etc/ipsec.secrets

sudo systemctl restart strongswan

# Setting up firewall rules
sudo ufw allow OpenSSH
sudo ufw enable
sudo ufw allow 500,4500/udp

# Finding the internet interface for setting up routing
public_int=$(ip route | grep default | cut -d' ' -f5)

# Altering the postrouting to fit our internet interface and chosen client IP range
sed -i "s/-A POSTROUTING \-s.*-o.*-m policy \-\-pol ipsec \-\-dir out \-j ACCEPT/-A POSTROUTING \-s $2\/24 \-o $public_int \-m policy \-\-pol ipsec \-\-dir out \-j ACCEPT/g" "before.rules"
sed -i "s/-A POSTROUTING \-s.*-o.*-j MASQUERADE/-A POSTROUTING \-s $2\/24 -o $public_int -j MASQUERADE/g" "before.rules"
sed -i "s/-A FORWARD --match policy --pol ipsec --dir in \-s.*-o.*-p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360/-A FORWARD --match policy --pol ipsec --dir in \-s $2\/24 -o $public_int -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360/g" "before.rules"
sed -i "s/-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d.*-j ACCEPT/-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d $2\/24 -j ACCEPT/g" "before.rules"
sed -i "s/-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d.*-j ACCEPT/-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d $2\/24 -j ACCEPT/g" "before.rules"


sudo mv /etc/sysctl.conf /etc/sysctl.conf.original
sudo cp sysctl.conf /etc/sysctl.conf

sudo mv /etc/ufw/before.rules /etc/ufw/before.rules.original
sudo cp before.rules /etc/ufw/before.rules

sudo ufw disable
sudo ufw enable


